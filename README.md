# ESS Olog Product

Packaging of ESS Olog service.

The build process pulls Olog service jar from Maven Central and creates a runnable jar. ESS specific dependencies
for the service (see pom.xml) are also included into the build artifact.