FROM openjdk:17

RUN touch /var/lib/misc/empty_tags.json /var/lib/misc/empty_logbook.json

COPY target/ess-olog-product-*.jar /opt/olog/olog.jar

RUN useradd -ms /bin/bash olog
USER olog

CMD ["java", "-Djavax.net.ssl.trustStore=/usr/java/openjdk-17/lib/security/cacerts","-Ddefault.tags.url=/var/lib/misc/empty_tags.json","-Ddefault.logbook.url=/var/lib/misc/empty_logbook.json", "-jar", "/opt/olog/olog.jar"]
